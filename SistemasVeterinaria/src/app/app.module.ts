import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FilterUsersPipe } from '../app/pipes/filter-users.pipe';
import { SelectDropDownModule } from 'ngx-select-dropdown';
import { ProviderComponent } from './Page/provider/provider.component';
import { HomeComponent } from './Page/home/home.component';
import { FormsModule } from '@angular/forms';
import { AgendaComponent } from './Page/agenda/agenda.component';
import { CalendarModule } from '@syncfusion/ej2-angular-calendars';
import { ProductComponent } from './Page/product/product.component';
import { SaleComponent } from './Page/sale/sale.component';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'agenda', component: AgendaComponent },
  { path: 'provider', component: ProviderComponent },
  { path: '', redirectTo: "/home", pathMatch: "full" },
  {path:'product',component:ProductComponent},
  {path:'sale', component: SaleComponent} 
];

@NgModule({
  declarations: [
    AppComponent,
    SaleComponent,
    FilterUsersPipe,
    ProviderComponent,
    HomeComponent,
    AgendaComponent,
    ProductComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SelectDropDownModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    FormsModule, CalendarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
