import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterUsers'
})
  export class FilterUsersPipe implements PipeTransform {
    transform(value: any[], args?: any): any {

      if (!value) return null;
      if (!args) return value;
  
      args = args.toLowerCase();
  
      return value.filter(function (item) {
        return JSON.stringify(item.name).toLowerCase().includes(args)||
        JSON.stringify(item.status).toLowerCase().includes(args)
      });
    }
    // transform(items: any[], searchText: string): any[] {
  
    //   if (!items) {
    //     return [];
    //   }
    //   if (!searchText) {
    //     return items;
    //   }
    //   searchText = searchText.toLocaleLowerCase();
  
    //   return items.filter(it => {
    //     return it.toLocaleLowerCase().includes(searchText);
    //   });
    // }
  }
    
