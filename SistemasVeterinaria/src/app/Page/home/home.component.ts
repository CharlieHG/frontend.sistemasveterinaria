import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  indice;
  public users=[
    {
      name:"Panfilomeno Pancracio",
      lastname:"Hermenegildo Fernandiozino",
      email:"panpanHerme@gmail.com",
      status:"Subscribed"
    },
    {
      name:"Cesar",
      lastname:"Zavala",
      email:"cesarzm98@gmail.com",
      status:"Subscribed"
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
