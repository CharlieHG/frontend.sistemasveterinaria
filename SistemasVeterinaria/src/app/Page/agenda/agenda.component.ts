import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit {
  //<---------Variables calendario---->
  //calendar: Date;
  //value:Date;
  //public value: Date[] =  [ new Date ];
  // public valueTable: Date[] = [new Date("10/14/2019"), new Date("10/15/2019")];
  //<------------Variables calendario---->
  
  //Public Search para pipe
  public search:any = '';
  
  //Cargar valores al formulario
  fechaFin = "2019-11-04T15:30:00";
  fechaInicio = "2019-11-04T15:30:00";
  phone = "6412352394";
  email = "ejemplo@gmail.com";
  service = "Vacuna";
  name = "Jesus Gutierrez De La Colina";
  pet= "paca";
  specie="Gato";
  age="4 meses";
  gender="Hembra";
  status= "En consulta";
  encargado= "Dr.Jesus Rivera";
  //Array model agenda
  public agenda: any[] = [
    {
      fechaFin: "2019-11-04T15:30:00",
      fechaInicio: "2019-11-04T14:00:00",
      phone: "6412352394",
      email: "ejemplo@gmail.com",
      service: "Vacuna",
      name: "Jesus Gutierrez De La Colina",
      pet: "paco",
      specie:"Perro",
      age:"2 años",
      gender:"Macho",
      status: "En proceso",
      encargado: "No asignado"
    },
    {
      fechaFin: "2019-11-04T17:30:00",
      fechaInicio: "2019-11-04T16:30:00",
      phone: "6412352394",
      email: "ejemplo2@gmail.com",
      service: "Peluqueria",
      name: "Maria De Los Cielos",
      pet: "paca",
      specie:"Gato",
      age:"4 meses",
      gender:"Hembra",
      status: "En consulta",
      encargado: "Dr.Jesus Rivera"
    },
    {
      fechaFin: "2019-11-04T19:30:00",
      fechaInicio: "2019-11-04T18:30:00",
      phone: "6412352394",
      email: "ejemplo3@gmail.com",
      service: "Peluqueria",
      name: "Maria De Los Cielos",
      pet: "Martin",
      specie:"Pato",
      age:"4 meses",
      gender:"Macho",
      status: "Cancelada",
      encargado: "Dr.Jesus Rivera"
    }
  ];

  //f
  ngOnInit() {
  }

  //Obtengo objeto seleccionado de la tabla
  prueba(p){
    console.log(p);
    
  }
  //Agrego nueva agenda
  add() {
    this.agenda.push({
      fechaFin:this.fechaFin,
      fechaInicio:this.fechaInicio,
      phone:this.phone,
      email:this.email,
      service:this.service,
      name:this.name,
      pet:this.pet,
      specie:this.specie,
      age:this.age,
      gender:this.gender,
      status:this.status,
      encargado:this.encargado
    });
    console.log(this.name);
  }
}
