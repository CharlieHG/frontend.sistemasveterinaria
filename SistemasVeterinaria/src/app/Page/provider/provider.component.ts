import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.css']
})
export class ProviderComponent implements OnInit {
  hola="hola";
  public products = [{
    id:1,
    name:"Comida para perro",
    detalle:"1.-Nutrición Canina Científica "+" 2.-Contiene vitaminas  "+" 3.-Contenido de alimentos naturales  "+"  4.-Fortalecido ",
    img:"https://images-na.ssl-images-amazon.com/images/I/616zLpqRSGL._AC_SX569_.jpg",
    precio:2532,
    tamano:"20kg",
    status:"Disponible"
  }
  ];

  constructor() { }

  ngOnInit() {
  }

}
