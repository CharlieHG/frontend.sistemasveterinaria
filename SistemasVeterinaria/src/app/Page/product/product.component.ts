import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor() { }
  detail;
  price;
  stock;
  provider;
  description;
  name;

  ngOnInit() {
  }

  usersList = [{
    detail: "BRAVECTO 4.5-10KG 250MG, ",
    price: "$493.00",
    stock: "1 caja - 10 kg",
    provider: "Farmaceutica Canina",
    description: "Desparasitante para cachorros.",
    name: "Ana Rodriguez",
  }]
}
