import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.css']
})
export class SaleComponent implements OnInit {
  SpecialityArray = [];
  QuotesArray = [];
  user;
  quoteStats = true;
  quote:any=[];
  price:number = 0;
  product = [];
  product1 = [];
  Speciality = [];
  ProductsArray = [];
  Speciality1 = [];
  config = {
    displayKey:"name",
    search:true,
    placeholder:'Selecciona un usuario', 
    noResultsFound: 'No se encontraron resultados',
    searchPlaceholder:'Buscar Usuario',
    searchOnKey: 'name'
    }
    configSpeciality = {
      displayKey:"name",
      search:true,
      placeholder:'Selecciona la atención o atenciones', 
      noResultsFound: 'No se encontraron resultados',
      searchPlaceholder:'Buscar atenciones',
      searchOnKey: 'name'
      }
      configQuote = {
        displayKey:"Client",
        search:true,
        placeholder:'Selecciona una cita', 
        noResultsFound: 'No se encontraron resultados',
        searchPlaceholder:'Buscar cita por nombre del cliente',
        searchOnKey: 'Client'
        }
      configProducts = {
        displayKey:"name",
        search:true,
        placeholder:'Selecciona un producto o productos', 
        noResultsFound: 'No se encontraron resultados',
        searchPlaceholder:'Buscar productos',
        searchOnKey: 'name'
        }
  // usersArray: Array = [];
  constructor() { }

  ngOnInit() {
    this.fillData();
  }
  fillData(){
    this.SpecialityArray = [{name:"Corte de pelo", price:500}, {name:"Uno", price:20}, {name:"Dos", price:5}];
    this.ProductsArray = [{name:"Champú para perro", price:100}, {name:"Repelente", price:20}];
    // this.usersArray = [{id: 1, name: 'Martin Lugo'}, {id: 2, name: 'Norma Coronado'}, {id: 3, name: 'Cesar Zavala'}, {id: 4, name: 'Carlos Vazquez'}];
    this.QuotesArray = [{Id:1, Client:"Martin", User:"Dr Cezar Zavala", Pet:"Firulais", Date:"", Price:500, Speciality:"Corte de pelo"}]
  }
saveSale(){
  console.log(this.Speciality);
}  
priceUpdater(){
  this.price = 0;
  if(this.quote!=null){
    this.price = this.price + Number(this.quote.Price);
  }
  this.Speciality.forEach(element => {
    this.price = this.price + Number(element.price);
  });
  this.Speciality1.forEach(element => {
    this.price = this.price + Number(element.price);
  });
  this.product.forEach(element => {
    this.price = this.price + Number(element.price);
  });
  this.product1.forEach(element => {
    this.price = this.price + Number(element.price);
  });
}
quoteStat(value){
  this.price = 0;
  this.Speciality = [];
  this.Speciality1 = [];
  this.quote = [];
  this.product = [];
  this.product1 = [];
  this.quoteStats = value;
  this.fillData();
}
}
