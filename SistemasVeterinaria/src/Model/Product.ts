import { Provider } from './Provider';

export class Product{
    id:number;
    isActive:boolean;
    name:string;
    description:string;
    price: number;
    stock: number;
    category:string;
    dateExpiry:Date;
    provider:Provider;
}