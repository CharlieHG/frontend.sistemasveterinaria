import { Product } from './Product';

export class Provider{
    id:number;
    isActive:boolean;
    businessName:string;
    phoneNumber:string;
    address:string;
    products:Array<Product>=[];

} 