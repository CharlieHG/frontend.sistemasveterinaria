import { Specie } from './Specie';

export class Pet{
    id:number;
    isActive:boolean;
    name:string;
    specie:Specie;
    birthDate:Date;
    gender:number;
    weight:number;
}