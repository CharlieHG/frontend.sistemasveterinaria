import {Pet} from './Pet';

export class Client {
    id:number;
    isActive:boolean;
    name:string;
    lastName:string;
    email:string;
    phoneNumber:string;
    pets: Array<Pet>=[];
  }