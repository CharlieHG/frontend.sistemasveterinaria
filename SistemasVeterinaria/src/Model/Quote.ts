export class Quote{
    id:number;
    isActive:boolean;
    idClient:number;
    idUser:number;
    idPet:number;
    quoteDate:Date;
    price:number;
}