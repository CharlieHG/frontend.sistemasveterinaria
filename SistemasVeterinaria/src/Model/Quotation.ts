import { ProductDetail } from './ProductDetail';

export class Quotation{
    id:number;
    isActive:boolean;
    idQuote:number;
    total:number;
    productDetails:Array<ProductDetail>=[];
}