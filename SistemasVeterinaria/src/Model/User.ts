import { SpecialtyArea } from './SpecialtyArea';

export class User{
    id:number;
    isActive:boolean;
    name:string;
    lastName:string;
    userName:string;
    roleEnum:number;
    area:SpecialtyArea
}