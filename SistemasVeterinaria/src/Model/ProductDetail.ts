import { Product } from './Product';

export class ProductDetail{
    id:number;
    isActive:boolean;
    quantity:number;
    idQuotation:number;
    products:Array<Product>=[];
}